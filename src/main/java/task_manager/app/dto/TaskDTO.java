package task_manager.app.dto;

import lombok.*;
import task_manager.app.model.Task;

import java.time.LocalDate;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class TaskDTO {
    private String id;
    private String name;
    private String status;
    private LocalDate ld_task;

    public static TaskDTO from(Task task) {
        return new TaskDTO(task.getId(), task.getName(), task.getStatus(), task.getLd_task());
    }
}