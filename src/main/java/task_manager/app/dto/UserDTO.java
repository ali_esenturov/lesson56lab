package task_manager.app.dto;

import lombok.*;
import task_manager.app.model.User;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDTO {
    private String name;
    private String email;

    public static UserDTO from(User user) {
        return new UserDTO(user.getName(), user.getEmail());
    }
}
