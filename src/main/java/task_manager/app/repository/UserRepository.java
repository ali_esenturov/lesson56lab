package task_manager.app.repository;

import org.springframework.data.repository.CrudRepository;
import task_manager.app.model.User;

public interface UserRepository extends CrudRepository<User, String> {
    User findUserByEmail(String email);
}