package task_manager.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import springfox.documentation.annotations.ApiIgnore;
import task_manager.app.model.Task;

@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, String> {
   Page<Task> findAllByUserId(@ApiIgnore Pageable pageable, String userId);
}