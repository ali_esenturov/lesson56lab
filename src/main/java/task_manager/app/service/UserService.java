package task_manager.app.service;

import org.springframework.stereotype.Service;
import task_manager.app.dto.UserDTO;
import task_manager.app.model.User;
import task_manager.app.repository.UserRepository;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByEmail(String email){
        return userRepository.findUserByEmail(email);
    }

    public UserDTO addUser(User user){
        userRepository.save(user);
        return UserDTO.from(user);
    }
}