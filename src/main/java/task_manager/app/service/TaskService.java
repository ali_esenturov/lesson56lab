package task_manager.app.service;

import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;
import task_manager.app.dto.TaskDTO;
import task_manager.app.model.Task;
import task_manager.app.repository.TaskRepository;
import task_manager.app.repository.UserRepository;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    public TaskService(TaskRepository taskRepository, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    public Iterable<TaskDTO> getMyTasks(@ApiIgnore Pageable pageable, Authentication authentication){
        String email = authentication.getName();
        String userId = userRepository.findUserByEmail(email).getId();
        return taskRepository.findAllByUserId(pageable, userId).map(TaskDTO::from);
    }

    public Task getTaskById(String taskId){
        return taskRepository.findById(taskId).get();
    }

    public String changeMyTask(String taskId, Authentication authentication){
        Task task = taskRepository.findById(taskId).get();
        String taskUserId = task.getUser().getId();
        String userId = userRepository.findUserByEmail(authentication.getName()).getId();
        if(userId.equals(taskUserId)){
            if(task.getStatus().equals("NEW")){
                task.setStatus("IN_PROCESS");
                taskRepository.save(task);
                return "Changed status is 'In process'";
            }
            else if(task.getStatus().equals("IN_PROCESS")){
                task.setStatus("COMPLETED");
                taskRepository.save(task);
                return "Changed status is 'Completed'";
            }
            else{
                return "Task is completed";
            }
        }
        else{
            return "it is not your task";
        }
    }
}