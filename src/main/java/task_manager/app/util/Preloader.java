package task_manager.app.util;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import task_manager.app.configuration.SecurityConfig;
import task_manager.app.model.Task;
import task_manager.app.model.User;
import task_manager.app.repository.TaskRepository;
import task_manager.app.repository.UserRepository;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class Preloader {
    private final Random r = new Random();


    @Bean
    public CommandLineRunner initDataBase(UserRepository userRepository, TaskRepository taskRepository){

        return (args) -> {
            userRepository.deleteAll();
            taskRepository.deleteAll();

            User user = new User("ali", "ali@gmail.com", SecurityConfig.encoder().encode("mypass"));
            userRepository.save(user);
            User user1 = new User("ali2", "ali2@gmail.com", SecurityConfig.encoder().encode("mypass2"));
            userRepository.save(user1);


            List<Task> tasks = new ArrayList<>();
            Task task = new Task(Generator.makeName(), Generator.makeDescription(),
                    LocalDate.now().plus(r.nextInt(10) + 1, ChronoUnit.DAYS), user, "NEW");
            Task task1 = new Task(Generator.makeName(), Generator.makeDescription(),
                    LocalDate.now().plus(r.nextInt(10) + 1, ChronoUnit.DAYS), user, "NEW");
            Task task2 = new Task(Generator.makeName(), Generator.makeDescription(),
                    LocalDate.now().plus(r.nextInt(10) + 1, ChronoUnit.DAYS), user, "NEW");
            Task task3 = new Task(Generator.makeName(), Generator.makeDescription(),
                    LocalDate.now().plus(r.nextInt(10) + 1, ChronoUnit.DAYS), user, "NEW");

            Task task4 = new Task(Generator.makeName(), Generator.makeDescription(),
                    LocalDate.now().plus(r.nextInt(10) + 1, ChronoUnit.DAYS), user1, "NEW");


            tasks.add(task);
            tasks.add(task1);
            tasks.add(task2);
            tasks.add(task3);
            tasks.add(task4);
            taskRepository.saveAll(tasks);

            System.out.println("done");
        };
    }
}