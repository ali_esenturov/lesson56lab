package task_manager.app.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.UUID;


@Data
@Document(collection = "tasks")
public class Task {
    @Id
    private String id = UUID.randomUUID().toString();

    private String name;
    private String description;
    private LocalDate ld_task;
    @DBRef
    private User user;
    private String status;

    public Task(String name, String description, LocalDate ld_task, User user, String status) {
        this.name = name;
        this.description = description;
        this.ld_task = ld_task;
        this.user = user;
        this.status = status;
    }
}