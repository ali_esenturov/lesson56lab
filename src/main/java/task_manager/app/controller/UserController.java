package task_manager.app.controller;

import org.springframework.web.bind.annotation.*;
import task_manager.app.dto.UserDTO;
import task_manager.app.model.User;
import task_manager.app.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/add")
    public UserDTO createUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @GetMapping("/user/{email}")
    public User getUserByEmail(@PathVariable String email){
        return userService.getUserByEmail(email);
    }
}