package task_manager.app.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import task_manager.app.dto.TaskDTO;
import task_manager.app.model.Task;
import task_manager.app.service.TaskService;

@RestController
@RequestMapping("/task")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/my_tasks")
    public Iterable<TaskDTO> getMyTasks(@ApiIgnore Pageable pageable, Authentication authentication){
        return taskService.getMyTasks(pageable, authentication);
    }

    @GetMapping("/details/{taskId}")
    public Task getDetailsOfTask(@PathVariable String taskId){
        return taskService.getTaskById(taskId);
    }

    @PutMapping("/change_my_task/{taskId}")
    public String changeMyTask(@PathVariable String taskId, Authentication authentication){
        return taskService.changeMyTask(taskId, authentication);
    }
}
